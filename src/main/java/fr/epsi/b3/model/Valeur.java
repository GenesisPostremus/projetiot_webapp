package fr.epsi.b3.model;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Valeur{
    private double value;
    @JsonFormat(pattern = "dd/MM/yyyy HH:mm:ss", timezone = "Europe/Paris")
    private Date date_value;

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public Date getDate_value() {
        return date_value;
    }

    public void setDate_value(Date date_value) {
        this.date_value = date_value;
    }

    public String getFormatedIntDate() {
        DateFormat jour = new SimpleDateFormat("yyyy-MM-dd");
        String jourString = jour.format(this.date_value);
        DateFormat secs = new SimpleDateFormat("HH:mm:ss");
        String secsString = secs.format(this.date_value);
        return jourString + "T" + secsString;
    }


}
