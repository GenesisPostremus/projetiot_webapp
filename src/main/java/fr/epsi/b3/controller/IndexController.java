package fr.epsi.b3.controller;

import fr.epsi.b3.dto.ErreurDto;
import fr.epsi.b3.model.Capteur;
import fr.epsi.b3.model.Device;
import fr.epsi.b3.model.Utilisateur;
import fr.epsi.b3.model.UtilisateurConnectionDto;
import fr.epsi.b3.service.UtilisateurService;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.io.IOException;

@Controller
@Scope("session")
public class IndexController {

    private UtilisateurService utilisateurService = new UtilisateurService();
    private Utilisateur utilisateur;

    @GetMapping({"/", "/connection"})
    public String afficherFormulaireConnection(Model model, UtilisateurConnectionDto utilisateurConnectionDto) throws IOException, ErreurDto {
        if (utilisateur != null) {
            this.utilisateur = utilisateurService.getUtilisateurFromAPI(utilisateur);
            model.addAttribute("utilisateur", this.utilisateur);
            return "accueil";
        }
        return "connection";
    }

    @GetMapping({"/logout"})
    public String deconnexion(Model model, UtilisateurConnectionDto utilisateurConnectionDto) {
        this.utilisateur = null;
        return "connection";
    }

    @PostMapping({"/connection"})
    public String connection(Model model, @Validated @ModelAttribute UtilisateurConnectionDto utilisateurDto, BindingResult bindingResult) throws InterruptedException, IOException {
        if (bindingResult.hasErrors()) {
            return "connection";
        }
        try {
            this.utilisateur = utilisateurService.getUtilisateurFromAPI(utilisateurDto);
        } catch (ErreurDto erreurDto) {
            ObjectError objectError = new ObjectError("identifiant", erreurDto.getMessage());
            bindingResult.addError(objectError);
            return "connection";
        }
        model.addAttribute("utilisateur", this.utilisateur);
        return "accueil";
    }

    @GetMapping({"/accueil"})
    public String accueil(Model model, UtilisateurConnectionDto utilisateurConnectionDto) throws IOException, ErreurDto {
        return afficherFormulaireConnection(model, utilisateurConnectionDto);
    }

    @GetMapping({"/details/{device}/{capteur}"})
    public String details(Model model, @PathVariable String device, @PathVariable String capteur) {
        try {
            this.utilisateur = utilisateurService.getUtilisateurFromAPI(utilisateur);
            Device returnedDevice = utilisateur.getDeviceFromMacAdresse(device);
            Capteur returnedCapteur = returnedDevice.getCapteurFromName(capteur);
            model.addAttribute("device", returnedDevice);
            model.addAttribute("capteur", returnedCapteur);
            return "details";
        } catch (Exception e) {
            return "accueil";
        }
    }
}
