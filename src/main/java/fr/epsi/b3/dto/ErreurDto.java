package fr.epsi.b3.dto;

public class ErreurDto extends Exception {
    public ErreurDto(String message) {
        super(message);
    }
}
