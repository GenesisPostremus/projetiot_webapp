package fr.epsi.b3.model;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class UtilisateurConnectionDto {
    @NotNull(message = "Entrer un id")
    private String user_name;
    @NotNull(message = "Entrer un mot de passe")
    private String password;

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
