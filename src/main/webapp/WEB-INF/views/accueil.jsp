<%@page pageEncoding="UTF-8" contentType="text/html" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="<c:url value="/resources/fontawesome-5.13.0/css/all.css"/>">
    <link rel="stylesheet" href="<c:url value="/resources/css/bootstrap.min.css"/>">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="<c:url value="/resources/js/bootstrap.min.js"/>"></script>
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/style.css"/>">
    <title>Accueil</title>
</head>
<html>
<body onbeforeunload="document.location.reload(true)">
<div class="container">
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="<c:url value="/"/>"><i class="fas fa-home"></i></a>
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav ml-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="<c:url value="/logout"/>"><i class="fas fa-sign-out-alt"></i></a>
                </li>
            </ul>
        </div>
    </nav>
    <div class="row">
        <div class="col-md-12 text-center">
            <div class="welcome-texte">
                <h1>Bienvenue <c:out value="${utilisateur.user_name.toUpperCase()}"></c:out></h1>
            </div>
        </div>
        <c:forEach var="device" items="${utilisateur.devices}">
            <div class="col-sm-1"></div>
            <c:forEach items="${device.capteurs}" var="capteur">
                <div class="col-sm-4">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title text-center">
                                <c:out value="Capteur : ${capteur.name}"></c:out>
                            </h5>
                            <p class="card-text text-center value-capteur">
                                <c:out value="${capteur.lastValeur.value}${capteur.unite}"></c:out>
                            </p>
                            <div class="text-center">
                                <a href="<c:url value="/details/${device.adresse_mac}/${capteur.name}"/>"
                                   class="btn btn-primary">Détails</a>
                            </div>
                        </div>
                    </div>
                </div>
                <c:if test="${device.capteurs.indexOf(capteur) == 0}">
                    <div class="col-sm-2"></div>
                </c:if>
            </c:forEach>
            <div class="col-sm-1"></div>
        </c:forEach>
    </div>
</div>
</body>
</html>
