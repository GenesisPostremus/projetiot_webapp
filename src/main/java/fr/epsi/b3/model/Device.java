package fr.epsi.b3.model;

import java.util.ArrayList;
import java.util.List;

public class Device {

    private String adresse_mac;
    private List<Capteur> capteurs = new ArrayList<>();

    public List<Capteur> getCapteurs() {
        return capteurs;
    }

    public void setCapteurs(List<Capteur> capteurs) {
        this.capteurs = capteurs;
    }

    public String getAdresse_mac() {
        return adresse_mac;
    }

    public void setAdresse_mac(String adresse_mac) {
        this.adresse_mac = adresse_mac;
    }

    public Capteur getCapteurFromName(String name) {
        Capteur returnedCapteur = null;
        for (Capteur c : this.getCapteurs()){
            if(c.getName().equals(name)){
                returnedCapteur = c;
            }
        }
        return returnedCapteur;
    }
}
