# Projet IOT webapp
Application web permettant d'afficher les résultats obtenus par des sondes

Pour reporter un bug ou des suggestions de fonctionnalités, contacter :
<thibaultnesti@gmail.com>

#Installation
## Pré-requis
Retrouver le repository [git](https://gitlab.com/GenesisPostremus/projetiot_webapp)

```
git clone https://gitlab.com/GenesisPostremus/projetiot_webapp.git
```
Paquets à installer pour Linux

```
apt-get install maven
apt-get install openjdk
```
## Configuration
Le projet est développé en respectant un architecture MVC.
Les données sont accédés en utilisant la classe DAO qui appel une API.

## Lancement de l'apllication

Installer un serveur [Tomcat](http://tomcat.apache.org/)
Entrer l'addresse du serveur et les identifiants de connexion Tomcat dans la configuration Maven.

Lancer la commande maven 
```
mvn clean package
```

L'application sera compilé dans un fichier *war* et déployer sur le serveur Tomcat.

#Contribution

Les **pulls requests** sont les bienvenues. Pour des changements majeurs merci d'ouvrir un *tag* pour en disctuer avant que vous ne fassiez des changements.

#Licence
[Apache](https://www.apache.org/licenses/LICENSE-2.0)





