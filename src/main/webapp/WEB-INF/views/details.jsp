<%@page pageEncoding="UTF-8" contentType="text/html" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="<c:url value="/resources/fontawesome-5.13.0/css/all.css"/>">
    <link rel="stylesheet" href="<c:url value="/resources/css/bootstrap.min.css"/>">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="<c:url value="/resources/js/bootstrap.min.js"/>"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.js"></script>
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/style.css"/>">
    <script type="text/javascript">
        window.onload = function () {

            var dps = [];
            var chart = new CanvasJS.Chart("chartContainer", {
                theme: "light2", // "light1", "dark1", "dark2"
                animationEnabled: true,
                title: {
                    text: "${capteur.fullName}"
                },
                axisX: {
                    valueFormatString: "DD/MM HH:mm:ss"
                },
                axisY: {
                    title: "${capteur.fullName}",
                    suffix: "${capteur.unite}"
                },
                data: [{
                    type: "line",
                    xValueType: "dateTime",
                    xValueFormatString: "DD/MM HH:mm:ss",
                    yValueFormatString: "#,##0 ${capteur.unite}",
                    dataPoints: dps
                }]
            });


            var xValue;
            var yValue;

            <c:forEach items="${capteur.valeurs}" var="valeur" varStatus="loop">
            xValue = Date.parse("${valeur.formatedIntDate}");
            yValue = parseFloat("${valeur.value}");
            dps.push({
                x: xValue,
                y: yValue
            });
            </c:forEach>
            chart.render();
            console.log(dps);

        }
    </script>
    <title>Accueil</title>
</head>
<html>
<body>
<div class="container">
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="<c:url value="/"/>"><i class="fas fa-home"></i></a>
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav ml-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="<c:url value="/logout"/>"><i class="fas fa-sign-out-alt"></i></a>
                </li>
            </ul>
        </div>
    </nav>
    <div class="row">
        <div class="col-md-12 text-center">
            <div class="welcome-texte">
                <h1>Détail <c:out value="${capteur.name}"></c:out></h1>
            </div>
        </div>
        <div class="col-md-1"></div>
        <div class="col-md-10" id="chartContainer" style="height: 370px;"></div>
        <div class="col-md-1"></div>
    </div>
    <div class="separateur"></div>
    <div class="row">
        <div class="col-md-5">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th scope="col">Valeur</th>
                    <th scope="col">Date</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${capteur.valeurs}" var="valeur">
                    <tr>
                        <td><c:out value="${valeur.value} ${capteur.unite}"/></td>
                        <td><c:out value="${valeur.date_value}"/></td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
        <div class="col-md-1"></div>
        <div class="col-md-4">
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">Valeur la plus haute</th>
                    <th scope="col">Valeur la plus basse</th>
                    <th scope="col">Moyenne</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td><c:out value="${capteur.maxValue.value} ${capteur.unite}"/></td>
                    <td><c:out value="${capteur.minValue.value} ${capteur.unite}"/></td>
                    <td><c:out value="${capteur.avgValeur} ${capteur.unite}"/></td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="col-md-1"></div>
    </div>
</div>
</body>
<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
</html>
