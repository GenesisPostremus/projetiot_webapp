package fr.epsi.b3.model;

import java.util.ArrayList;
import java.util.List;

public class Utilisateur {

    private String user_name;
    private String user_email;
    private String password;
    private List<Device> devices = new ArrayList<>();

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getUser_email() {
        return user_email;
    }

    public void setUser_email(String user_email) {
        this.user_email = user_email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<Device> getDevices() {
        return devices;
    }

    public void setDevices(List<Device> devices) {
        this.devices = devices;
    }

    public Device getDeviceFromMacAdresse(String adresseMac) {
        Device returnedDevice = null;
        for (Device d : this.getDevices()
        ) {
            if (d.getAdresse_mac().equals(adresseMac)) {
                returnedDevice = d;
            }
        }
        return returnedDevice;
    }
}
