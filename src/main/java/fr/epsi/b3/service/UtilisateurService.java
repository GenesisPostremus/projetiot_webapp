package fr.epsi.b3.service;

import fr.epsi.b3.dao.UtilisateurDao;
import fr.epsi.b3.dto.ErreurDto;
import fr.epsi.b3.model.Utilisateur;
import fr.epsi.b3.model.UtilisateurConnectionDto;

import java.io.IOException;

public class UtilisateurService {

    private UtilisateurDao utilisateurDao = new UtilisateurDao();

    public Utilisateur getUtilisateurFromAPI(UtilisateurConnectionDto utilisateurConnectionDto) throws ErreurDto, IOException {
        return utilisateurDao.getUtilisateurFromAPI(utilisateurConnectionDto);
    }
    public Utilisateur getUtilisateurFromAPI(Utilisateur utilisateur) throws ErreurDto, IOException {
        return utilisateurDao.getUtilisateurFromAPI(utilisateur);
    }

}
