package fr.epsi.b3.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Capteur {

    private String name;
    private String unite;
    private List<Valeur> valeurs = new ArrayList<>();

    public List<Valeur> getValeurs() {
        return valeurs;
    }

    public void setValeurs(List<Valeur> valeurs) {
        this.valeurs = valeurs;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUnite() {
        return unite;
    }

    public void setUnite(String unite) {
        this.unite = unite;
    }

    public Valeur getLastValeur(){
        return valeurs.get(valeurs.size()-1);
    }

    public Valeur getMinValue(){
        return Collections.min(this.valeurs, Comparator.comparing(v -> v.getValue()));
    }

    public Valeur getMaxValue(){
        return Collections.max(this.valeurs, Comparator.comparing(v -> v.getValue()));
    }

    public double getAvgValeur(){
        int tot = 0;
        for (Valeur v: this.valeurs
             ) {
            tot += v.getValue();
        }
        return Math.round(tot/this.valeurs.size());
    }

    public String getFullName(){
        if (name.equals("TEMP")){
            return "Température";
        }
        return "Humidité";
    }
}
