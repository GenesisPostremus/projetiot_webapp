package fr.epsi.b3.dao;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import fr.epsi.b3.dto.ErreurDto;
import fr.epsi.b3.model.HttpRequest;
import fr.epsi.b3.model.Utilisateur;
import fr.epsi.b3.model.UtilisateurConnectionDto;

import java.io.IOException;

public class UtilisateurDao {


    public Utilisateur getUtilisateurFromAPI(UtilisateurConnectionDto utilisateurConnectionDto) throws IOException, ErreurDto {
        Gson gson = new GsonBuilder().setDateFormat("dd/MM/yyyy HH:mm:ss").create();
        String jsonString = gson.toJson(utilisateurConnectionDto);
        HttpRequest request = new HttpRequest("http://ec2-18-222-202-250.us-east-2.compute.amazonaws.com:8080/projet_iot_api/user");
        return (Utilisateur) request.httpPost(jsonString);
    }

    public Utilisateur getUtilisateurFromAPI(Utilisateur utilisateur) throws IOException, ErreurDto {
        UtilisateurConnectionDto utilisateurConnectionDto = new UtilisateurConnectionDto();
        utilisateurConnectionDto.setUser_name(utilisateur.getUser_name());
        utilisateurConnectionDto.setPassword(utilisateur.getPassword());
        return this.getUtilisateurFromAPI(utilisateurConnectionDto);
    }

}
