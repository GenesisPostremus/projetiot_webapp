<%@page pageEncoding="UTF-8" contentType="text/html"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="<c:url value="/resources/css/bootstrap.min.css"/>">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="<c:url value="/resources/js/bootstrap.min.js"/>"></script>
  <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/style.css"/>">
  <title>Se connecter</title>
</head>
<body>
<div class="login-form">
  <form:form servletRelativeAction="/connection" modelAttribute="utilisateurConnectionDto" acceptCharset="utf-8">
    <h2 class="text-center">Se connecter</h2>
    <div class="form-group">
      <form:input type="text" class="form-control" placeholder="Username" path="user_name"/>
      <form:errors path="user_name"/>
    </div>
    <div class="form-group">
      <form:input type="password" class="form-control" placeholder="Password" path="password"/>
      <form:errors path="password"/>
    </div>
    <div class="form-group">
      <button type="submit" class="btn btn-primary btn-block">Connexion</button>
    </div>
    <form:errors path="*"/>
  </form:form>
</div>
</body>
</html>